'use strict';

const config = require('./fractal/_config');

const path = require('path');
const fractal = module.exports = require('@frctl/fractal').create();


/*
 * Fractal configuration
 */
fractal.set('project.title', config.title);

fractal.components.set('path', path.join(__dirname, config.components));

fractal.components.set('ext', config.extension);

fractal.components.set('statuses', config.statuses);

fractal.components.set('default.status', config.defaultStatus);

if (config.displayBoundries) {
    fractal.components.set('default.display', config.displayBoundries);
}

fractal.docs.set('path', path.join(__dirname, config.docSrc));

fractal.web.set('static.path', path.join(__dirname, config.staticAssets));

fractal.web.set('builder.dest', path.join(__dirname, config.staticHtml));

    // prevent double reload on browsersync
fractal.web.set('server.syncOptions', {
    watchOptions: {
        ignored: path.join(__dirname, '**/*.scss'),
    },
});


/*
 * Web UI
 */
const mandelbrot = require('@frctl/mandelbrot');
const myCustomisedTheme = mandelbrot({
    favicon: config.favicon,
    styles: [
        'default',
        '/css/fractal-web-ui.css'
    ],
    panels: config.theme.panels,
});
fractal.web.theme(myCustomisedTheme);


/*
 * Customising Handlebars
 */
const handlebars = require('@frctl/handlebars')();

    // set as the default template engine for components
fractal.components.engine(handlebars);

    // use the same instance for documentation
fractal.docs.engine(handlebars);


/*
 * Add Handlebar helpers for comparing elements
 */
const handlebarHelpers = require('./fractal/handlebarHelpers/handlebarHelpers');

const instance = fractal.components.engine();

instance.handlebars.registerHelper('compare', handlebarHelpers.compare);
instance.handlebars.registerHelper('include', handlebarHelpers.include);

/*
 * Add Handlebar helpers for Markdown
 * https://github.com/helpers/handlebars-helpers
 */

const helpers = require('handlebars-helpers')();

instance.handlebars.registerHelper('markdown', helpers.markdown);

/*
 * Command: Create new component
 */
const newComponent = require('./fractal/newComponent/newComponent');
fractal.cli.command('new component', newComponent.new);


/*
 * Command: List all components
 */
const listComponents = require('./fractal/listComponents/listComponents');
fractal.cli.command('list components', listComponents.list);
