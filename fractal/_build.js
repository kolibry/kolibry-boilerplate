//***** Fractal *****//

const settings = {};

const fractal = require('../fractal.config.js'),
      logger = fractal.cli.console;

const gulp = require('gulp'),
      gulpPlumber = require('gulp-plumber');

const gulpAutoprefixer = require('gulp-autoprefixer'),
      gulpSass = require('gulp-sass')(require('sass')),
      gulpSourcemaps = require('gulp-sourcemaps');

const rollup = require('rollup'),
      rollupResolve = require('@rollup/plugin-node-resolve'),
      gulpBabel = require('gulp-babel'),
      gulpFile = require('gulp-file'),
      gulpMinify = require('gulp-minify'),
      rollupCommonjs = require('@rollup/plugin-commonjs');

const init = function(options) {

    options = options || {};

    if (typeof options === "string") {

        options = {
            dist: options
        };

    }

    settings.dist = options.dist || './static/';

}

   //Start the Fractal server

const start = function() {

    const server = fractal.web.server({
        sync: true
    });

    server.on('error', err => logger.error(err.message));

    return server.start().then(() => {
        logger.success(`Fractal server is now running at ${server.url}`);
    });

}

    // Run a static export of the project web UI

const styles = function() {

    return (
        gulp.src([
                './fractal/*.scss',
            ],
            { allowEmpty: true }
        )
        .pipe(gulpPlumber())
        .pipe(gulpSourcemaps.init({
            loadMaps: true
        }))
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(gulpAutoprefixer())
        .pipe(gulpSourcemaps.write('./'))
        .pipe(gulp.dest(settings.dist + '/css/'))
    );

}

const scripts = function() {

    return rollup
        .rollup({
            input: './fractal/fractal-doc.js',
            output: {
                format: 'cjs',
            },
            plugins: [
                rollupResolve.nodeResolve(),
                rollupCommonjs()
            ]
        })
        .then(bundle => {
            return bundle.generate({
                // sourcemap: true
            });
        })
        .then(generate => {

            const output = generate.output.shift();

            return gulpFile('fractal-doc.js', output.code, { src: true })
                .pipe(gulpBabel({
                    presets: ['@babel/env'],
                }))
                .pipe(gulpMinify({
                    ext: {
                        src: '.js',
                        min: '.min.js'
                    },
                    preserveComments: 'some',
                }))
                .pipe(gulp.dest(settings.dist + '/js/'));

        });

}

const build = function() {

    const builder = fractal.web.builder();

    builder.on('progress', (completed, total) => logger.update(`Exported ${completed} of ${total} items`, 'info'));
    builder.on('error', err => logger.error(err.message));

    return builder.build().then(() => {
        logger.success('Fractal build completed!');
    });

};

module.exports = {
    init: init,
    start: start,
    styles: styles,
    scripts: scripts,
    build: build,
};
