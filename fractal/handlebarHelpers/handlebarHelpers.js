/*
 * Helper for comparing elements
 *   "if var is value then do something"
 *
 * Source: http://doginthehat.com.au/2012/02/comparison-block-helper-for-handlebars-templates/#comment-44
 *
 * {{#compare type "textarea"}}
 * Default comparison of "==="
 * {{/compare}}

 * {{#compare slots ">" 5}}
 * There are more than 5 slots
 * {{/compare}}
 *
 * {{#compare type "~=" "text, password, email"}}
 * Type is in list
 * {{/compare}}
 *
 */

module.exports = {

    compare: function(lvalue, operator, rvalue, options) {

        var operators, result;

        if (arguments.length < 3) {
            throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
        }

        if (options === undefined) {
            options = rvalue;
            rvalue = operator;
            operator = "===";
        }

        operators = {
            '==': function (l, r) {
                return l == r;
            },
            '===': function (l, r) {
                return l === r;
            },
            '!=': function (l, r) {
                return l != r;
            },
            '!==': function (l, r) {
                return l !== r;
            },
            '<': function (l, r) {
                return l < r;
            },
            '>': function (l, r) {
                return l > r;
            },
            '<=': function (l, r) {
                return l <= r;
            },
            '>=': function (l, r) {
                return l >= r;
            },
            'typeof': function (l, r) {
                return typeof l == r;
            },
            '~=': function (l, r) {
                return r.split(/[,; ] ?/).indexOf(l) !== -1;
            },
        };

        if (!operators[operator]) {
            throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
        }

        result = operators[operator](lvalue, rvalue);

        if (result) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }

    },

    include: function(filepath, options) {

        const fs = require('fs');

        function readFile(filepath) {

            let embed = `<code class="doc-note">Include '${filepath}' not found!</code>`;

            try {
                embed = fs.readFileSync(filepath, 'utf8');
            } catch (error) {

                if (typeof filepath === "string" && ! filepath.match(/^static\//)) {

                    // Remove "../" from path and prepend "static/"
                    filepath = filepath.replace(/^(\.{0,2}\/)+/, "");

                    // Prepend "static/"
                    filepath = "static/" + filepath;

                    embed = readFile(filepath);

                }

            }

            return embed;

        }

        let embed = readFile(filepath);

        return embed;

    },

};
