module.exports = {

    // Command: list all components
    list: function (args, done)
    {
        const app = this.fractal;
        for (let item of app.components.flatten()) {
            this.log(`${item.handle} - ${item.status.label}`);
        }
        done();
    }

};