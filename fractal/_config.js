module.exports = {

    // Title of the project
    title: 'Kolibry Boilerplate',

    // Directory, where the components live:
    components: 'components',

    // File extension for all component view templates:
    extension: '.html',

    statuses: {
        prototype: {
            label: "Prototype",
            description: "Do not implement.",
            color: "#bbb"
        },
        wip: {
            label: "WIP",
            description: "Work in progress. Implement with caution.",
            color: "#FF9233"
        },
        review: {
            label: "Review",
            description: "In review. Almost ready to implement.",
            color: "#A2ECA2"
        },
        ready: {
            label: "Ready",
            description: "Ready to implement.",
            color: "#29CC29"
        },
        deprecated: {
            label: "Deprecated",
            description: "Will be removed, do not implement.",
            color: "#333"
        }
    },
    // Default status to apply to all components (prototype, wip or ready):
    defaultStatus: 'wip',

    // Boundries for width of preview rendering area
    displayBoundries: {
        'min-width': '320px',
        'max-width': 'none',
    },

    // Directory, where the documentation pages live:
    docSrc: 'docs',

    // Directory, where static assets live:
    staticAssets: 'static',

    // Directory, where Fractal exports the static html builds to:
    staticHtml: 'public',

    // Directory, where the favicon for the pattern library lives:
    favicon: '/favicon.svg',

    // Panels to be shown in the library:
    theme: {
        panels: [
            "notes", "html", "info", "view", "context", "resources",
        ]
    }

};
