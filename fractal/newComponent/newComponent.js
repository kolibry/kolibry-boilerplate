module.exports = {

    new: function (args, done) {
        const inquirer = require('inquirer');
        const fs = require('fs');
        const fsExtra = require('fs-extra');
        const console = this.console;

        const root = 'components/';

        const getTemplate = {
            scss: (component) => {
                return `@import "_${component.name}.settings";
@import "_${component.name}.styles";
`
            },
            js: (component) => {
                return `import ${component.name} from "./_${component.name}.script.js";

${component.name}.init();
`
            },
            html: (component) => {
                return `<div class="${component.name}{{#modifier}} {{.}}{{/modifier}}">
    Prototype for component “${component.name}”
</div>
`
            },
            yaml: (component) => {
                let content = `status: prototype
label: ${component.label}
`;
                if (component.type == 'template') {
                    content += `meta:
    template-modifier: is-${component.name}
`;
                }
                return content;
            },
            settings: (component) => {
                return `\$${component.className}_styles: (
    font-family: monospace,
    background-color: lightgray,
) !default;
`           },
            styles: (component) => {
                return `%${component.className} {
    @include styles(\$${component.className}_styles);
}

.${component.className} {
    @extend %${component.className};
}
`           },
            script: (component) => {
                return `export default (function (){
    const defaults = {
        foo: "${component.name}"
    };

    function init() {
        console.log(defaults.foo);
    }

    return {
        init: init
    }
})();
`
            },
        };

        const collections = {

            base: {
                root: root + '01-base',
                scss: '_base.scss',
                js: '_base.js',
            },

            element: {
                root: root + '02-elements',
                scss: '_elements.scss',
                js: '_elements.js',
            },

            fragment: {
                root: root + '03-fragments',
                scss: '_fragments.scss',
                js: '_fragments.js',
            },

            module: {
                root: root + '04-modules',
                scss: '_modules.scss',
                js: '_modules.js',
            },

            template: {
                root: root + '05-templates',
                scss: '_templates.scss',
                js: '_templates.js',
            },

            sample: {
                root: root + '06-samples',
                scss: '_samples.scss',
                js: '_samples.js',
            },

        };

        console.br().log('Creating new component. Just a few questions:').br();

        const QUESTIONS = [
            {
                name: 'componentType',
                type: 'list',
                message: 'What is the TYPE of the new component?',
                choices: Object.keys(collections),
                default: 0
            }, {
                name: 'componentName',
                type: 'input',
                message: 'What is the NAME of the new component?',
                suffix: 'Note: You can use slashes to create subdirectories.',
                validate: (answer) => { return answer !== "" },
            }, {
                name: 'createJs',
                type: 'list',
                message: 'JavaScript file needed:',
                choices: ['no', 'yes'],
                default: 0
            }
        ];

        return inquirer.prompt(QUESTIONS).then(function (answers) {

            const jsNeeded = answers['jsNeeded'];

                // Select collection

            let collection = collections[answers['componentType']];

                // Define component

            let component = {
                type: answers['componentType'],
                name: answers['componentName'],
                createJs: answers['createJs'] === "yes",
            }

                // Directory and name

            component.path = [];

            let matches = component.name.match(/\//);

            if (matches) {
                component.path = component.name.split(/\//);
                component.name = component.path.pop();

                component.path.push("");
            }

            if (component.type === "sample"){

                if (! component.name.match(/Samples$/)){
                    component.name += "Samples";
                } else if (component.name.match(/Sample$/)){
                    component.name += "s";
                }

            }

            component.dir = collection.root + "/" + component.path.join("/") + component.name + '/';

                // Label

            component.label = createLabel(component.name);

                // Class name

            component.className = `${component.name}`;

            if (component.type === "template"){
                component.className = `is-${component.name}`;
            }

                // Define collection files

            collection.files = {
                scss: {
                    path: collection.root + "/" + collection.scss,
                    content: '\n@import "' + component.path.join("/") + component.name + '/_' + component.name + '";',
                    message: `./${collection.root}/_${component.type}s.scss updated`,
                },
                js: {
                    path: collection.root + "/" + collection.js,
                    content: '\nimport "./' + component.path.join("/") + component.name + '/_' + component.name + '";',
                    message: `./${collection.root}/_${component.type}s.js updated`,
                },
            };

                // Define component files

            component.files = {

                scss: {
                    path: component.dir + '_' + component.name + '.scss',
                    content: getTemplate.scss(component),
                    message: `./${component.dir}_${component.name}.scss created`,
                },
                js: {
                    path: component.dir + '_' + component.name + '.js',
                    content: getTemplate.js(component),
                    message: `./${component.dir}_${component.name}.js created`,
                },
                html: {
                    path: component.dir + component.name + '.html',
                    content: getTemplate.html(component),
                    message: `./${component.dir}${component.name}.html created`,
                },
                yaml: {
                    path: component.dir + component.name + '.config.yaml',
                    content: getTemplate.yaml(component),
                    message: `./${component.dir}${component.name}.config.yaml created`,
                },
                settings: {
                    path: `${component.dir}_${component.name}.settings.scss`,
                    content: getTemplate.settings(component),
                    message: `./${component.dir}_${component.name}.settings.scss created`,
                },
                styles: {
                    path: `${component.dir}_${component.name}.styles.scss`,
                    content: getTemplate.styles(component),
                    message: `./${component.dir}_${component.name}.styles.scss created`,
                },
                scripts: {
                    path: `${component.dir}_${component.name}.script.js`,
                    content: getTemplate.script(component),
                    message: `./${component.dir}_${component.name}.script.js created`,
                },

            };

                // If no JavaScript is needed delete file definitions

            if (! component.createJs) {
                delete component.files.js;
                delete component.files.scripts;
            }

                // If type is sample remove styling
                // FIXME: Should also remove scripts by default, but the prompt
                //        prompt always ask for it.

            if (component.type === "sample") {
                delete component.files.scss;
                delete component.files.settings;
                delete component.files.styles;
            }

                // Check if component name already exists

            if (fs.existsSync(component.dir)) {
                console.error('Cannot create component: ' + component.name + ' already exists');
                done();
            }

                // Create directories

            fsExtra.mkdirsSync(component.dir);

                // Create component files

            console.log('\nGenerating component structure in ./' + collection.root + ':\n');

            for (let type in component.files){

                let file = component.files[type];

                fsExtra.appendFile(file.path, file.content, function (err) {

                    if (err) throw err;

                    console.success(file.message);

                });

            }

                // Update collection files

            if (component.files.scss){

                fsExtra.appendFile(collection.files.scss.path, collection.files.scss.content, function (err) {

                    if (collection.root !== collections.base.dir) {
                        sortLines(collection.files.scss.path);
                        console.success(collection.files.scss.message);
                    }

                    if (err) throw err;

                });

            }

            if (component.files.scripts) {

                fsExtra.appendFile(collection.files.js.path, collection.files.js.content, function (err) {

                    sortLines(collection.files.js.path);

                    console.success(collection.files.js.message);


                    if (err) throw err;

                });
            }

            done();

        });

        function createLabel(name) {
            /*
             * Insert a space in front of all capital letters + uppercase the first character
             */

            return name.replace(/([A-Z])/g, ' $1')
                .replace(/^./, function(str){ return str.toUpperCase(); });

        }

        function sortLines(filename) {
            /*
             * Sort lines with imports alphabetically
             *   (Imports will always be moved to the end of the file!)
             */

            var content = fsExtra.readFileSync(filename).toString(),
                lines = content.split('\n');

            const imports = lines.filter(element => element.match(/^@?import /));

            const others = lines.filter(element => ! imports.includes(element));

            imports.sort();

                // Join, remove leading whitespace and append empty line

            var content = others.join('\n') + imports.join('\n').trim() + '\n';

            fsExtra.writeFile(filename, content, function (err) {
                if (err) throw err;
            });

        }

    }

};
