import ClipboardJS from "clipboard";

export default (function (){

    function init() {
        toggleClass();
        toggleVisibility();
    }

        /*
         * Enter browsers edit mode
         */

    var enable_editing = getParameterByName("edit");

    if (enable_editing !== null && enable_editing != 'false' && enable_editing != '0') {
        document.designMode = "on";
    }

        /*
         * Set text direction
         */

    var direction = getParameterByName("rtl");

    if (direction !== null && direction != 'false' && direction != '0') {
        document.querySelector("html").dir = "rtl";
    }

        /*
         * Show layout grid
         *   Can set by query parameter `grid`
         *   or the class `show-grid` on body tag
         */

    var show_grid = document.querySelectorAll("body.show-grid").length !== 0 || getParameterByName("grid");

    var addGrid = function(){

        var grid = document.createElement("div"),
            page = document.createElement("div"),
            container = document.createElement("div"),
            row = document.createElement("div");

        document.body.appendChild(grid);
        grid.classList.add("doc-layout-grid");

        page.classList.add("grid-page");
        grid.appendChild(page);

        container.classList.add("grid-container");
        page.appendChild(container);

        row.classList.add("grid-row");
        container.appendChild(row);

        for (var i = 0; i < 12; i++){
            var col = document.createElement("div");
            col.classList.add("grid-column");
            row.appendChild(col);
        }

        return grid;

    }

    if ( show_grid !== null && show_grid != 'false' && show_grid != '0') {

        var containers = document.querySelectorAll(".doc-layout-grid");

        if (containers.length === 0){

            var containers = [
                addGrid()
            ];

        }

        for (var i = 0; i < containers.length; i++){
            var container = containers[i];
            container.classList.add("is-visible");
        }

    }

        /* Get parameter by name */

    function getParameterByName(name){
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&'); // Mask `[` and `]` width backslashed

        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);

        if (!results) return null;

        if (!results[2]) return '';

        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

        /* Toggle class */

    const toggleClass = function(selector, classname) {

        if (typeof classname !== "string") {
            return;
        }

        let nodes = getNodes(selector);

        for (var i = 0; i < nodes.length; i++) {
            const node = nodes[i];
            node.classList.toggle(classname);
        }

    }

        /* Toggle data attribute */

    const toggleDataAttribute = function(selector, dataAttributeName) {

        if (typeof dataAttributeName !== "string") {
            return;
        }

        let nodes = getNodes(selector);

        for (var i = 0; i < nodes.length; i++) {

            const node = nodes[i];
            let status = (node.dataset[dataAttributeName] !== "true");

            node.dataset[dataAttributeName] = status;

        }

    }

        /* Toggle visibility */

    const toggleVisibility = function(selector) {

        let nodes = getNodes(selector);

        for (var i = 0; i < nodes.length; i++) {

            const node = nodes[i],
                  hidden = node.getAttribute("aria-hidden") === "true";

            if (hidden) {
                node.setAttribute("aria-hidden", "false");
            } else {
                node.setAttribute("aria-hidden", "true");
            }

        }

    }

    const getNodes = function(selector) {

        let nodes = [];

        if (typeof selector === "undefined") {
            return nodes;
        }

        if (typeof selector.getAttribute !== "undefined") {
            nodes = [selector];
        }

        if (typeof selector === "string") {
            nodes = document.querySelectorAll(selector);
        }

        return nodes;

    }

    const setTheme = function(element, options) {
        /*
         * @param {node} element – Event trigger
         * @param {node} options.container – Selector for element that contains targets, if empty ".doc-variant" will be used
         * @param {string} options.value – Name of theme to apply
         * @param {string} options.target – Selector for elements theme will applied to
         */

        if (typeof options !== "object") {
            options = {};
        }

        if (typeof element === "undefined" || typeof options.target !== "string" || typeof options.value !== "string") {
            console.log("setTheme: Options missing!");
            return;
        }

        if (typeof options.container !== "string") {
            options.container = ".doc-variant";
        }

        const container = element.closest(options.container);

        if (! container) {
            return;
        }

        let theme = options.value;

        if (theme === "default") {
            theme = "";
        }

        const targets = container.querySelectorAll(options.target);

        for (let i = 0; i < targets.length; i++) {
            const target = targets[i];
            target.dataset.theme = theme;
        }

    }

        /* Copy data to clipboard */

    const clipboard = new ClipboardJS(
        "*[data-copy-text]",
        {
            text: function(trigger) {
                return trigger.dataset.copyText;
            }
        }
    );

    clipboard.on('success', function(e) {
        e.trigger.dataset.copiedText = e.trigger.dataset.copyText;
    });

        /***** Export public methods *****/

    return {
        init: init,
        toggleClass: toggleClass,
        toggleDataAttribute: toggleDataAttribute,
        toggleVisibility: toggleVisibility,
        setTheme: setTheme,
    };

})();
