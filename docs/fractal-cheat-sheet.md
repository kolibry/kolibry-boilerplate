---
title: Fractal Cheat Sheet
order: 4
---

Die HTML-Templates basieren auf der Markup-Sprache *Handlebars* (siehe [Dokumentation](https://handlebarsjs.com/)). Die dazugehörigen Kontext-Daten werden in *YAML* notiert (siehe [Wikipedia](https://de.wikipedia.org/wiki/YAML)).

## Komponenten einbinden

**Achtung:** Das Label der Komponente ist *nicht* in Camel-Case (auch wenn das Verzeichnis in Camel-Case benannt ist).

### Ohne Kontext einbinden

```hbs
\{{> @subcomponent }}
```

### Mit Kontext einbinden

Wenn zum Darstellen der eingebundenen Komponente deren Kontext-Daten gebraucht werden, muss der Handlebars-Helper `render` benutzt werden:

```hbs
\{{render '@subcomponent' }}
```

Über den @-Helper kann der komplette Kontext einer anderen Komponente übernommen werden:

```yaml
label: mycomponent
context:
    subcomponent: '@subcomponent'
```

Sollen die Kontext-Daten der eingebundenen Komponente überschrieben werden, muss der Komponente ein Objekt aus den Kontext-Daten übergeben werden:

```yaml
label: mycomponent
context:
    subcomponent:
        title: Hello World!
```

```hbs
\{{render '@subcomponent' (contextData '@mycomponent' subcomponent)}}
```

Sollen die Kontext-Daten mit denen der Sub-Komponente zusammengeführt werden, muss `merge=true` angehängt werden:

```hbs
\{{render '@subcomponent' (contextData '@mycomponent' subcomponent) merge=true}}
```

### Kontext übernehmen

Über die Punkt-Syntax kann in einer Variante oder einer anderen Komponente ein Kontext übernommen werden:

```yaml
label: myelement
context:
    salutation: Hello World!
```

```yaml
label: mycomponent
context:
    salutation: '@myelement.salutation'
```

#### Komponente mehrfach einbinden

Wenn mehrere Instanzen eine Komponente eingebunden werden sollen, können die Kontext-Daten mit dem Schlüsselwort `this` übergeben werden:

```yaml
label: mycomponent
context:
    subcomponents:
        -
            title: Hello World!
        -
            title: Hallo Welt!
        -
            title: Bonjour tout le monde !
```

```hbs
\{{#subcomponents}}
    \{{render '@subcomponent' (contextData '@mycomponent' this)}}
\{{/subcomponents}}
```

Sollen die Instanzen eine Komponente unverändert eingebunden werden, kann man deren kompletten Kontext übernehmen:

```yaml
label: mycomponent
context:
    subcomponents:
        - `@subcomponent`
        - `@subcomponent`
        - `@subcomponent`
```

## Assets einbinden

Beim Einbinden von Assets (z.B. Bildern) muss der Handlebars-Helper `path` benutzt werden:

```hbs
<img src="\{{path '/images/my-image.jpg'}}" alt=""/>
```

**Über Kontext-Daten:**

```yaml
context:
    src: /images/my-image.jpg
```
```hbs
<img src="\{{path src}}" alt=""/>
```
Oder:

```hbs
    \{{#src}}
<img src="\{{path .}}" alt=""/>
    \{{/src}}
```

## Auf Daten einer Komponente zugreifen

**Name der Komponente**

```hbs
<code>Namen der Komponente: \{{_self.name}}</code>
```
