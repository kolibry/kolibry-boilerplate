---
title: Namenskonventionen
order: 3
---

+ Der Name muss aus mindestens zwei Wörtern bestehen, z.B. *„Image-Gallery“* (anstatt *„Gallery“*) oder *„Teaser-Card“* (statt *„Teaser“*).
+ Die Wörter werden mit einem Bindestrich verbunden (z.B. nicht *„Image Gallery“*).
+ Der Name darf nicht den Typ der Komponente als Bestandteil haben (z.B. nicht *„Gallery-Module“*).
+ Der Name sollte möglichst nicht das Aussehen beschreiben (z.B. nicht *„Large-Teaser-Card“*); siehe auch Kapitel *„Varianten“*.

**Achtung:** Der Name für eine Basis-Komponente oder ein Element bestehen nur aus einem Wort z.B. *„Color“* oder *„Button“*.

### Varianten

Gibt es Varianten einer Komponente, dann werden zu ihrer Benennung der Name der ursprünglichen Komponente erweitert.

+ Der Name der Variation wird durch einen Schrägstrich getrennt angehängt, z.B. *„Teaser-Card / Large“*.
+ Der Name der Variation soll den Unterschied zur ursprünglichen Komponente möglichst kurz beschreiben, z.B. *„Teaser-Card / Image“* (nicht *„Teaser Card / with Teaser Image“*)
+ Es können auch mehrere Variationen miteinander kombiniert werden; dazu werden die Namen der Variationen durch Kommata getrennt angehängt z.B. *„Teaser-Card / Large, Image, Disabled“*.

## Umsetzung im Quellcode

**Schema:**

+ Name in camelCase: `.myComponent` (anstatt `.my-component`)
+ Elemente der Komponente mit Bindestrich anhängen: `.myComponent-componentElement`
+ bei Eindeutigkeit tiefere Verschachtelungen vermeiden also z.B. `.myComponent-link` (anstatt `.myComponent-list-item-link`)

**Beispiel:**

```html
<nav class="mainNav">
	<ul class="mainNav-list">
		<li class="mainNav-item">
			<a class="mainNav-link">…</a>
		</li>
	</ul>
</nav>
```

### Varianten

**Schema:**

+ beginnt mit einem Verb: `is-`, `has-`
+ Name mit Bindestrichen: `.is-my-variant`

**Beispiele:**

```html
<div class="box is-inverted"></div>

<figure class="teaserImage is-desktop-only"></figure>

<article class="teaser has-cta">
	<a href="./">
		<span class="teaser-cta">…</span>
	</a>
</article>

```
