---
title: Component Library
---

Dies ist die Entwicklungs- und Dokumentationsplattform für digitale Produkte. Mit ihrer Hilfe werden alle Komponenten, die zum Erstellen von User Interfaces zur Verfügung stehen, entwickelt, bewertet, getestet, dokumentiert und ausgeliefert. Sie ist die Referenz für das HTML-Markup und die zugrundeliegende Datenstruktur jeder einzelnen Komponenten und ihrer Beziehungen untereinander.

Die Component Library basiert auf dem Framework *Fractal* (siehe [Fractal User Guide](https://fractal.build/guide/). Die Anleitung zum lokalen Setups ist im [Readme des Repositories](https://gitlab.com/vitronic-web-ui/design-library/blob/master/readme.md) dokumentiert.

### Struktur

Die Komponenten der Library sind wie folgt strukturiert:

1. **Base:** Grundlegende Gestaltungsrichtlinien wie z.B. Vorgaben für Farbe, Typografie oder Gestaltungsraster
1. **Elements:** Kleinste Bausteine des UI wie z.B. Überschriften, Textabsätze oder Buttons
1. **Fragments:** Bausteine zusammengesetzt aus mehreren Elementen, die aber noch kein eigenständiges Modul sind, wie z.B. Gruppe von Buttons oder Eingabefeld mit Label und Hinweis.
1. **Modules:** Bausteine zusammengesetzt aus mehreren Elementen und/oder Fragmenten, die eigenständig auf einer Page funktionieren, wie z.B. Navigation oder Kontaktformular.
1. **Templates:** Vorlagen für komplette Pages zusammengesetzt aus einzelnen Modulen.

### Stand der Entwicklung

Die Component Library ist stetig in Entwicklung. Der aktuelle Status jeder Komponente wird im Menü durch einen farbigen Punkt und durch das Label rechts oben in der Detailansicht der Komponente angezeigt.

<table>
    <tbody>
        <tr>
            <th>Label</th>
            <th>Beschreibung</th>
        </tr>
        <tr>
            <td>
                <div class="Status Status--tag">
                    <label class="Status-label" style="background-color: #bbb; border-color: #bbb;">Prototype</label>
                </div>
            </td>
            <td>Nicht verwenden.</td>
        </tr>
        <tr>
            <td>
                <div class="Status Status--tag">
                    <label class="Status-label" style="background-color: #FF9233; border-color: #FF9233;">WIP</label>
                </div>
            </td>
            <td>In der Entwicklung (nur in Entwicklungsumgebung verwenden).</td>
        </tr>
        <tr>
            <td>
                <div class="Status Status--tag">
                    <label class="Status-label" style="background-color: #A2ECA2; border-color: #A2ECA2;">Review</label>
                </div>
            </td>
            <td>Im Abstimmungsprozess (nur in Entwicklungsumgebung verwenden).</td>
        </tr>
        <tr>
            <td>
                <div class="Status Status--tag">
                    <label class="Status-label" style="background-color: #29CC29; border-color: #29CC29;">Ready</label>
                </div>
            </td>
            <td>Kann implementiert werden.</td>
        </tr>
        <tr>
            <td>
                <div class="Status Status--tag">
                    <label class="Status-label" style="background-color: #333; border-color: #333;">Deprecated</label>
                </div>
            </td>
            <td>Nicht mehr verwenden (kann in zukünftigen Versionen eventuell entfernt werden).</td>
        </tr>
    </tbody>
</table>

### Werkzeuge

#### Gestaltungsraster einblenden

Über den Parameter `grid` kann das Gestaltungsraster eingeblendet werden. Der Parameter wird mit einem vorangestellten Fragezeichen an die URL angehängt, z.B.:

`/components/preview/copy?grid`

#### Schreibrichtung ändern

Über den Parameter `rtl` kann die Schreibrichtung auf von rechts nach links umgestellt werden, z.B.:

`/components/preview/copy?rtl`

#### Editiermodus für Texte

Über den Parameter `edit` kann in den Editiermodus gewechselt werden; in diesem Modus können die Texte zu Textzwecken temporär überschrieben werden. Der Parameter wird mit einem vorangestellten Fragezeichen an die URL angehängt, z.B.:

`/components/preview/copy?edit`

Zum Verlassen des Editiermodus muss die Vorschau ohne Parameter neu geladen werden.
