---
title: Technische Umsetzung
order: 3
---

## Assets

Alle Assets – also Bilder, Styles, Scripte, Fonts und Favicons – werden in den Komponenten definiert und abgelegt. Über einen Build-Prozess können die Assets zusammengesetzt, gerendert und kopiert werden. Das Build-Prozess lässt sich über npm starten:

```bash
npm run dev
```

Beim Build-Prozess beinhaltet im wesentlichen folgende Teilprozesse:

+ Bilder (JPG, PNG und SVG), Favicons und Font-Files werden unverändert kopiert.
+ Sass-Dateien werden zusammengeführt und minifiziert.
+ CSS3- und CSS4-Features werden über den Autoprefixer abwärtskompatibel notiert.
+ JavaScript-Dateien werden zusammengeführt und minifiziert.
+ ES6-Features werden über Babel abwärtskompatibel gemacht.

## Komponenten der Component Library

> **Vorrangiges Ziel ist es, alle Komponenten möglichst modular und unabhängig voneinander aufzubauen.**

### Hierachie

Die Komponenten sollen möglichst unabhängig voneinander funktionieren. Deshalb ist es wichtig, dass sie nur von Teilen andere Komponenten abhängig sein dürfen, die in der Hierarchie unter ihnen stehen; z.B. darf eine Komponente des Typs *Elements* nur auf Sass-Variablen, -Helper oder JavaScript-Module von Komponenten *Base* und *Helpers zugreifen*.

Ausnahme dieser Regel sind in eine Markup-Vorlage komplett eingebundene Komponenten, z.B. ein Fragment *formField*, dass in ein Fragment *formFieldset* eingebunden ist. Hier muss trotzdem darauf geachtet werden, dass die Komponente *formFieldset* keine Sass-Variablen, -Helper oder JavaScript-Module der Komponente *formField* einbindet.

### Aufbau einer Komponente

Die Teile einer Komponente sind wie folgt organisiert:

+ Jede Komponente besteht mindestens zwei Dateien:
    + einer YAML-Datei mit der Konfiguration und den Kontext-Daten
    + einer Markup-Vorlage (Handlebars mit der Dateiendung `.html`)
+ Das Styling wird mindestens über eine Sass-Datei definiert (idealerweise sollen die Styles selbst aber auch wieder über mehrere Dateien organisiert werden; siehe unten).
+ Die Frontend-Funktionalitäten werden mindestens über eine JavaScript-Datei definiert (idealerweise sollen sie in Modulen organisiert werden).

Beispiel für den minimalen Aufbau einer Komponente:

```bash
+-- myComponent/
|   +-- myComponent.config.yaml
|   +-- myComponent.html
|   +-- _myComponent.scss
|   +-- _myComponent.js (optional)
```

### Komponente einbinden

Die Sass- und JavaScript-Dateien der Komponente müssen in die entsprechenden Dateien der Komponenten-Gruppe eingebunden werden, also z.B für das Fragment *myComponent*:

JavaScript-Datei des Komponenten-Gruppe (z.B. `_fragments.js`):

```js
import "./myComponent/_myComponent";
```

Sass-Datei des Komponenten-Gruppe (z.B. `_fragments.scss`):

```scss
@import "myComponent/_myComponent";
```

### CLI-Befehl

Im CLI von Fractal kann man mit dem Befehl `new component` eine neue Komponente erzeugen, die schon alle Einzelteile enthält und in die Library eingebunden wird.

## Konfiguration und Kontext

Siehe Dokumentation bei Fractal:

+ [Configuration Files | Fractal](https://fractal.build/guide/core-concepts/configuration-files.html#configuration-file-formats)
+ [Context Data | Fractal](https://fractal.build/guide/core-concepts/context-data.html#defining-using-context-data)


## Markup-Vorlage

Die Markup-Vorlage der Komponente wird in der Markup-Sprache *Handlebars* notiert (siehe [Dokumentation](https://handlebarsjs.com/)).

Der Grundaufbau der Komponenten des Typs *Fragment* und *Modules* folgt meist dem folgenden Schema:

```hbs
<div class="myComponent">
    Hallo Welt!
</div>
```

Um Varianten eine Komponente über eine zusätzliche Klasse, der sogenannten *Modifier Class*, stylen zu können, wird diese Klasse meist direkt als Option mit notiert:

```yaml
context:
    modifier: is-special-variant
```

```hbs
<div class="myComponent{{#modifier}} {{.}}{{/modifier}}">
    Hallo Welt!
</div>
```

## Optionale Frontend-Funktionalitäten (JavaScript)

Frontend-Funktionalitäten sollten (ab einem gewissen Grad an Komplexität) oder müssen, wenn sie auf Drittanbieter-Scripte aufbauen, über Module organisiert werden. Diese Module werden als *CommonJS*-Module aufgebaut und eingebunden.

```bash
+-- myComponent/
|   +-- _myComponent.js
|   +-- js/
|   |   +-- _myVendorScript.js
|   |   +-- _scripts.js
```

Die Module werden in die übergeordnete Script-Datei (z.B. `_myComponent.js`) in alphabetischer Reihenfolge importiert:

```js
import "./js/_myVendorScript";
import "./js/_scripts";
```

## Organisation des Stylings

Das Styling jeder Komponente sollte (ab einem gewissen Grad an Komplexität) in drei Bestandteile aufgeteilt werden:

1. seine Methoden (*Helpers*)
1. die Einstellungen (*Settings*)
1. die Style-Angaben (*Styles*)

Die drei Bestandteile der Komponente werden in einem Verzeichnis gesammelt, dass den Namen der Komponente (von einem Unterstrich angeführt) trägt. Zusätzlich gibt es eine Sass-Datei mit dem gleichen Namen; sie dient dazu die Bestandteile zusammenzufassen und die Komponente zu individualisieren.

```bash
+-- myComponent/
|   +-- _myComponent.scss
|   +-- scss/
|   |   +-- _helpers.scss
|   |   +-- _settings.scss
|   |   +-- _styles.scss
```

Die Bestandteile werden in einer festen Reihenfolge in die übergeordnete Styling-Datei (z.B. `_myComponent.scss`) importiert:

```
@import "_myComponent/_helpers";
@import "_myComponent/_settings";
@import "_myComponent/_styles";
```

### Methoden (`_helpers.scss`)

In der Datei `_helpers.scss` werden die Sass-Mixins und -Funktionen zusammengefasst, die in der Komponente verwendet werden. Alle Methoden einer Komponente haben den Komponentennamen als Präfix, z.B.:

```
@function myComponent_get-lighter-color($color){
    @return lighten($color, 10%);
}

@mixin myComponent_set-lighter-color($color){
    color: myComponent_get-lighter-color($color);
}
```

### Einstellungen (`_settings.scss`)

In der Datei `_settings.scss` werden alle Einstellungen (Variablen) der Komponente festgelegt. Damit jede Variable an übergeordneter Stelle überschrieben werden kann, müssen sie mit `!default` gekennzeichnet sein.

Bei der Benennung der Variablen sind folgende Namenskonventionen zu beachten:

+ Alle Variablen haben den Komponentennamen als Präfix (siehe [Naming]({{path '/docs/naming'}})).
+ Wenn die Variable für eine spezielle CSS-Eigenschaft steht, dann muss sie auch genau diese Eigenschaft als Namen tragen, also z.B. `$myComponent_background-color` (anstatt `$myComponent_color` oder `$myComponent_bkg`).

```
$myComponent_color: red !default;
$myComponent_hide-labels: true !default;
```

### Style-Angaben (`_styles.scss`)

In der Datei `_styles.scss` wird das eigentliche Styling definiert, z.B.:

```
.myComponent {

   a:hover {
      @include myComponent_set-lighter-color($myComponent_color);
   }

   labels {

      @if $myComponent_hide-labels {
         display: none;
      }

   }

}

```

### Anpassen des Stylings

Jede Komponente sollte so aufgebaut sein, dass die grundlegenden Anpassungen über Variablen vorgenommen werden können.

Dadurch, dass alle Variablen in der Einstellungs-Datei mit `!default` gekennzeichnet sind, kann jede einzelne Variable in der Datei `_myComponent.scss` „überschrieben“ werden, z.B.:

```
$myComponent_color: blue;

@import "_myComponent/_helpers";
@import "_myComponent/_settings";
@import "_myComponent/_styles";
```
