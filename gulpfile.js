'use strict';

const chalk = require('chalk'),
      gulp = require('gulp');

const defaults = {
    dist: "./static/",
    components: {
        src: "./components/",
        dist: "./static/",
    },
    icons: {
        dist: "./static/images/icons", // Has to be relative to "./components/icons.build.js"
    },
    fractal: {
        dist: "./static/",
    },
    docs: {
        dist: "./static/docs/",
    },
}

let config;

try {
    config = require("./_build.config.json");
} catch(e) {
    console.info(chalk.cyan(`build: Using default configuration.`));
}

const settings = Object.assign({}, defaults, config);

//***** Fractal *****//
//* TODO: If "settings.fractal.dist" is empty, skip build process

const fractal = require("./fractal/_build.js");

fractal.init({
    dist: settings.fractal.dist,
});

exports['fractal:assets'] = gulp.parallel(fractal.styles, fractal.scripts);
exports['fractal:build'] = gulp.series(gulp.parallel(fractal.styles, fractal.scripts), fractal.build);

exports['fractal:start'] = fractal.start;

//***** Components *****//

const components = require("./components/_build.js");

components.init(settings.components);

for (let name in components.tasks) {
    exports["components:" + name] = components.getTask(name);
}

exports["components:watch"] = components.watch;

exports.components = gulp.parallel(components.getAllTasks());


//***** Sync *****//

exports.sync = gulp.series(fractal.start, components.watch);


//***** Defaults *****//

exports.default = gulp.series(gulp.parallel(fractal.styles, fractal.scripts, components.getAllTasks()));
